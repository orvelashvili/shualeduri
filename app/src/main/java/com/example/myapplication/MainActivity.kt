package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.isDigitsOnly


class MainActivity : AppCompatActivity() {
    private lateinit var text1: TextView

    private lateinit var text2: TextView


    private lateinit var numbers: TextView

    private lateinit var tecNumber: TextView


    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text1= findViewById(R.id.text1)

        numbers = findViewById(R.id.numbers)

        text2 = findViewById(R.id.text2)

        tecNumber = findViewById(R.id.tecNumber)



    }

    fun register(clickedView: View) {

        if (numbers.text.isDigitsOnly() && numbers.text.length == 3 && tecNumber.text.length == 9 ) {

            if (numbers.text.toString() != "000") {

                Toast.makeText(applicationContext, "თქვენ-გავლილი გაქვთ ტექ-დათვალიერება", Toast.LENGTH_SHORT).show()

            }

        } else {
            Toast.makeText(applicationContext, "არარსებობს", Toast.LENGTH_SHORT).show()
        }

    }

}